angular.module('hack-esg').config(function($locationProvider) {
    $locationProvider.hashPrefix('');
});

angular.module('hack-esg').config(function($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'views/home.html',
        controller: 'homeController'
    });
});
